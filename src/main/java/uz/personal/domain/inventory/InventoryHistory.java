package uz.personal.domain.inventory;

import lombok.*;
import uz.personal.domain.Auditable;
import uz.personal.domain.product.Product;
import uz.personal.domain.product.Store;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "history")
public class InventoryHistory extends Auditable {
   private Long count;
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "product_id",nullable = false)
//    private List<Product> product;
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "store_id",nullable = false)
//    private List<Store> stores;

}
