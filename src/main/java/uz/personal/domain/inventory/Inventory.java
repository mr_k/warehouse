package uz.personal.domain.inventory;

import lombok.*;
import uz.personal.domain.Auditable;
import uz.personal.domain.product.Product;
import uz.personal.domain.product.Store;
import uz.personal.domain.warehouse.Warehouse;

import javax.persistence.*;
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "inventory")
public class Inventory extends Auditable {
//    @Column(nullable = false)
    private String Description;
@ManyToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "product_id",nullable = false)
private Product product;

}
