package uz.personal.domain.warehouse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.domain.Auditable;
import uz.personal.domain.product.Store;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "warehouse")
public class Warehouse extends Auditable {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String location;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "store_id")
    private Store store;
}
