package uz.personal.domain.client;

import lombok.*;
import uz.personal.domain.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "kompany")
public class Company extends Auditable {
    @Column(nullable = false)
    private String companyName;
    @Column(nullable = false, unique = true)
    private String phoneNumber;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String address;
    @Column(nullable = false)
    private String interest;
    @Column
    private String primaryStateRegistrationNumber;
    @Column
    private String iNN;
    @Column
    private String checkpointNumber;
    @Column
    private String legalAddress;
    @Column
    private String director;
    @Column
    private String nameOfTheBank;
    @Column
    private String accountNumber; // R/S
    @Column
    private String cBAN;  //customer's bank account number //K/S
    @Column
    private String bankIdentificationCode;    //BIK
    @Column(nullable = false, unique = true)
    private String discountCard;
    @Column
    private Double discountOnService;
    @Column
    private String note;
    @Column
    private String tags;
}
