package uz.personal.domain.client;

import lombok.*;
import uz.personal.domain.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "clients")
public class Client extends Auditable {
    @Column(nullable = false)
    private String clientName;
    @Column(nullable = false,unique = true)
    private String phoneNumber;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String address;
    @Column(nullable = false)
    private String interest;
    @Column(nullable = false, unique = true)
    private String discountCard;
//    @Column(nullable = false)
    private Double discountOnService;
    @Column
    private String note;
    @Column
    private String tags;
}
