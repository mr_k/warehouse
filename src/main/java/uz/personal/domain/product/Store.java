package uz.personal.domain.product;

import lombok.*;
import uz.personal.domain.Auditable;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "store")
public class Store extends Auditable {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String location;
}
