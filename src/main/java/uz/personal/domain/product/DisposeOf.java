package uz.personal.domain.product;

import lombok.*;
import uz.personal.domain.Auditable;
import uz.personal.domain.warehouse.Warehouse;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "disposeOf")
public class DisposeOf extends Auditable {
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private Double quantity;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Product product;

}
