package uz.personal.domain.product;

import lombok.*;
import uz.personal.domain.Auditable;
import uz.personal.domain.warehouse.Warehouse;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "products")
public class Product extends Auditable {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String category;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private String subcategory;
    @Column(nullable = false)
    private Double size;
//    @Column(nullable = false)
    private String barcode;
    @Column(nullable = false)
    private String vendorCode;
    @Column(nullable = false)
    private Double quantity;
    @Column
    private Double uzs_price;
    @Column
    private Double delivery_price_usd;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;
}
