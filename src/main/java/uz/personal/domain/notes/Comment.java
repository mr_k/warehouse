package uz.personal.domain.notes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.domain.Auditable;
import uz.personal.domain.product.Store;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="comment")
public class Comment extends Auditable {
//    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String comment;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "file_id")
    private File file;
}
