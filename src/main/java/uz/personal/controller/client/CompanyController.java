package uz.personal.controller.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.client.ClientCriteria;
import uz.personal.criteria.client.CompanyCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.client.*;
import uz.personal.response.DataDto;
import uz.personal.service.client.IClientService;
import uz.personal.service.client.ICompanyService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "company controller", tags = "company-controller")
@RestController
@Slf4j
public class CompanyController extends ApiController<ICompanyService> {

    public CompanyController(ICompanyService service) {
        super(service);
    }
    @ApiOperation(value = "Get Single Company")
    @RequestMapping(value = API_PATH + V_1 + "/company/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<CompanyDto>> getCompany(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Companies")
    @RequestMapping(value = API_PATH + V_1 + "/company/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<CompanyDto>>> getAllCompanies(@Valid CompanyCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Company Create")
    @RequestMapping(value = API_PATH + V_1 + "/company/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createCompany(@RequestBody CompanyCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Company Update")
    @RequestMapping(value = API_PATH + V_1 + "/company/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<CompanyDto>> updateCompany(@RequestBody CompanyUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Company Delete")
    @RequestMapping(value = API_PATH + V_1 + "//delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteCompany(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }}
