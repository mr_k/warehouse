package uz.personal.controller.client;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.client.ClientCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.client.ClientCreateDto;
import uz.personal.dto.client.ClientDto;
import uz.personal.dto.client.ClientUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.client.IClientService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "client controller", tags = "client-controller")
@RestController
@Slf4j
public class ClientController extends ApiController<IClientService> {
    public ClientController(IClientService service) {
        super(service);
    }
    @ApiOperation(value = "Get Single Client")
    @RequestMapping(value = API_PATH + V_1 + "/client/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<ClientDto>> getClient(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Clients")
    @RequestMapping(value = API_PATH + V_1 + "/client/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<ClientDto>>> getAllClients(@Valid ClientCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Client Create")
    @RequestMapping(value = API_PATH + V_1 + "/client/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createClient(@RequestBody ClientCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Client Update")
    @RequestMapping(value = API_PATH + V_1 + "/client/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<ClientDto>> updateClient(@RequestBody ClientUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Client Delete")
    @RequestMapping(value = API_PATH + V_1 + "/client/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteClient(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}

