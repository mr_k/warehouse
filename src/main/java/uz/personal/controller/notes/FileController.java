package uz.personal.controller.notes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.notes.FileCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.notes.FileCreateDto;
import uz.personal.dto.notes.FileDto;
import uz.personal.dto.notes.FileUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.notes.IFileService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "file controller", tags = "file-controller")
@RestController
public class FileController extends ApiController<IFileService> {
    public FileController(IFileService service) {
        super(service);
    }

    @ApiOperation(value = "Get Single File")
    @RequestMapping(value = API_PATH + V_1 + "/file/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<FileDto>> getFile(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Files")
    @RequestMapping(value = API_PATH + V_1 + "/files/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<FileDto>>> getAllFile(@Valid FileCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "File Create")
    @RequestMapping(value = API_PATH + V_1 + "/file/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createFile(@RequestBody FileCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "File Update")
    @RequestMapping(value = API_PATH + V_1 + "/file/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<FileDto>> updateFile(@RequestBody FileUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "File Delete")
    @RequestMapping(value = API_PATH + V_1 + "/file/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteFile(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}

