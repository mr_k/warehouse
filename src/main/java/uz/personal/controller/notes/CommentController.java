package uz.personal.controller.notes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.notes.CommentCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.notes.CommentCreateDto;
import uz.personal.dto.notes.CommentDto;
import uz.personal.dto.notes.CommentUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.notes.ICommentService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "comment controller", tags = "comment-controller")
@RestController
public class CommentController extends ApiController<ICommentService> {
    public CommentController(ICommentService service){ super(service);}
    @ApiOperation(value = "Get Single Comment")
    @RequestMapping(value = API_PATH + V_1 + "/comment/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<CommentDto>> getComment(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Comments")
    @RequestMapping(value = API_PATH + V_1 + "/comments/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<CommentDto>>> getAllComments(@Valid CommentCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Comment Create")
    @RequestMapping(value = API_PATH + V_1 + "/comment/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createComment(@RequestBody CommentCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Comment Update")
    @RequestMapping(value = API_PATH + V_1 + "/comment/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<CommentDto>> updateComment(@RequestBody CommentUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Comment Delete")
    @RequestMapping(value = API_PATH + V_1 + "/comment/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteComment(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}
