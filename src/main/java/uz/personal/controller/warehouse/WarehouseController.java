package uz.personal.controller.warehouse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.warehouse.WarehouseCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.warehouse.WarehouseCreateDto;
import uz.personal.dto.warehouse.WarehouseDto;
import uz.personal.dto.warehouse.WarehouseUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.warehouse.IWarehouseService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Warehouse controller", tags = "Warehouse-controller")
@RestController
public class WarehouseController extends ApiController<IWarehouseService> {
    public WarehouseController(IWarehouseService service) {
        super(service);
    }

    @ApiOperation(value = "Get Single Warehouse")
    @RequestMapping(value = API_PATH + V_1 + "/warehouse/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<WarehouseDto>> getWarehouse(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Warehouses")
    @RequestMapping(value = API_PATH + V_1 + "/warehouse/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<WarehouseDto>>> getAllWarehouses(@Valid WarehouseCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Warehouse Create")
    @RequestMapping(value = API_PATH + V_1 + "/warehouse/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createWarehouse(@RequestBody WarehouseCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Warehouse Update")
    @RequestMapping(value = API_PATH + V_1 + "/warehouse/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<WarehouseDto>> updateWarehouse(@RequestBody WarehouseUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Warehouse Delete")
    @RequestMapping(value = API_PATH + V_1 + "/warehouse/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteWarehouse(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}
