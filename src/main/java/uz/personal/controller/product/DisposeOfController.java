package uz.personal.controller.product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.product.DisposeOfCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.*;
import uz.personal.response.DataDto;
import uz.personal.service.product.IDisposeOfService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "disposeOf controller", tags = "disposeOf-controller")
@RestController
public class DisposeOfController extends ApiController<IDisposeOfService> {
    public DisposeOfController(IDisposeOfService service) {
        super(service);
    }
    @ApiOperation(value = "Get Single DisposeOf")
    @RequestMapping(value = API_PATH + V_1 + "/disposeOf/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<DisposeOfDto>> getDisposeOf(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List ")
    @RequestMapping(value = API_PATH + V_1 + "/disposeOf/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<DisposeOfDto>>> getAllDisposeOf(@Valid DisposeOfCriteria criteria) {
        return service.getAll(criteria);
    }
    @ApiOperation(value = "Dispose Create")
    @Transactional
    @RequestMapping(value = API_PATH + V_1 + "/disposeOf/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createDisposeOf(@RequestBody DisposeOfCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "DisposeOf Update")
    @Transactional
    @RequestMapping(value = API_PATH + V_1 + "/disposeOf/update", method = RequestMethod.PUT)
    public ResponseEntity<DataDto<DisposeOfDto>> updateDisposeOf(@RequestBody DisposeOfUpdateDto dto)
    {
        return service.update(dto);
    }


    @ApiOperation(value = "DisposeOf Delete")
    @RequestMapping(value = API_PATH + V_1 + "/disposeOf/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteDisposeOf(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
    }


