package uz.personal.controller.product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.product.StoresCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.*;
import uz.personal.response.DataDto;
import uz.personal.service.product.IStoreService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Stores controller", tags = "stores-controller")
@RestController
public class StoreController extends ApiController<IStoreService> {
    public StoreController(IStoreService service) {
        super(service);
    }
    @ApiOperation(value = "Get Single Store")
    @RequestMapping(value = API_PATH + V_1 + "/Store/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<StoreDto>> getStore(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Products")
    @RequestMapping(value = API_PATH + V_1 + "/Store/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<StoreDto>>> getAllStores(@Valid StoresCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Store Create")
    @RequestMapping(value = API_PATH + V_1 + "/stores/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createStores(@RequestBody StoreCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Store Update")
    @RequestMapping(value = API_PATH + V_1 + "/store/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<StoreDto>> updateStores(@RequestBody StoreUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Store Delete")
    @RequestMapping(value = API_PATH + V_1 + "/store/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteStores(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}
