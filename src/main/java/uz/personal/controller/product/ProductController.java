package uz.personal.controller.product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.personal.controller.ApiController;
import uz.personal.criteria.product.ProductCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.ProductCreateDto;
import uz.personal.dto.product.ProductDto;
import uz.personal.dto.product.ProductUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.product.IProductService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.*;
import java.net.URL;
import java.util.List;

@Api(value = "product controller", tags = "product-controller")
@RestController
public class ProductController extends ApiController<IProductService> {
    public ProductController(IProductService service) {
        super(service);
    }

    @ApiOperation(value = "Get Single Product")
    @RequestMapping(value = API_PATH + V_1 + "/product/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<ProductDto>> getProduct(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Products")
    @RequestMapping(value = API_PATH + V_1 + "/product/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<ProductDto>>> getAllProducts(@Valid ProductCriteria criteria) {
        return service.getAll(criteria);
    }

    @RequestMapping(value = API_PATH + V_1 + "/product/downloads", method = RequestMethod.GET)
    public ResponseEntity<Resource> downloadFile(HttpServletRequest request) {
        URL url = getClass().getClassLoader().getResource("example.xlsx");
        assert url != null;
        Resource resource = new UrlResource(url);
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @ApiOperation(value = "Product Create")
    @RequestMapping(value = API_PATH + V_1 + "/product/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createProduct(@RequestBody ProductCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Product Update")
    @RequestMapping(value = API_PATH + V_1 + "/product/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<ProductDto>> updateProduct(@RequestBody ProductUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Product Edit")
    @RequestMapping(value = API_PATH + V_1 + "/product/edit", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<ProductDto>> editProduct(@RequestBody ProductUpdateDto dto) {
        return service.edit(dto);
    }

    @ApiOperation(value = "Product Updates")
    @RequestMapping(value = API_PATH + V_1 + "/product/updates", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<List<ProductDto>>> updateProducts(@RequestBody List<ProductUpdateDto> productUpdateDtoList) {
        return service.updates(productUpdateDtoList);
    }

    @ApiOperation(value = "Product Delete")
    @RequestMapping(value = API_PATH + V_1 + "/product/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteProduct(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }

    @ApiOperation(value = "Products Create")
    @RequestMapping(value = API_PATH + V_1 + "/products/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createProducts(@RequestBody MultipartFile multipartFile, @RequestParam Long warehouseId) throws IOException {
        return service.creates(multipartFile, warehouseId);
    }


}
