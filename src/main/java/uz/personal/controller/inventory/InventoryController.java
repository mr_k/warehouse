package uz.personal.controller.inventory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.personal.controller.ApiController;
import uz.personal.criteria.inventory.InventoryCriteria;
import uz.personal.dto.GenericDto;
import uz.personal.dto.inventory.InventoryCreateDto;
import uz.personal.dto.inventory.InventoryDto;
import uz.personal.dto.inventory.InventoryUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.inventory.IInventoryService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Inventory controller", tags = "Inventory-controller")
@RestController
public class InventoryController extends ApiController<IInventoryService> {
    public InventoryController(IInventoryService service) {
        super(service);
    }
    @ApiOperation(value = "Get Single Inventory")
    @RequestMapping(value = API_PATH + V_1 + "/inventory/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<InventoryDto>> getInventory(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Inventories")
    @RequestMapping(value = API_PATH + V_1 + "/inventory/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<InventoryDto>>> getAllInventories(@Valid InventoryCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Inventory Create")
    @RequestMapping(value = API_PATH + V_1 + "/inventory/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createInventory(@RequestBody InventoryCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Inventory Update")
    @RequestMapping(value = API_PATH + V_1 + "/inventory/update", method = RequestMethod.PUT)
    @Transactional
    public ResponseEntity<DataDto<InventoryDto>> updateInventory(@RequestBody InventoryUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Inventory Delete")
    @RequestMapping(value = API_PATH + V_1 + "/inventory/delete/{id}", method = RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<DataDto<Boolean>> deleteInventory(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}
