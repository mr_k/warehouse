package uz.personal.service.notes;

import uz.personal.criteria.notes.CommentCriteria;
import uz.personal.domain.notes.Comment;
import uz.personal.dto.notes.CommentCreateDto;
import uz.personal.dto.notes.CommentDto;
import uz.personal.dto.notes.CommentUpdateDto;
import uz.personal.service.IGenericCrudService;

public interface ICommentService  extends IGenericCrudService<Comment, CommentDto, CommentCreateDto, CommentUpdateDto,Long, CommentCriteria> {
}
