package uz.personal.service.notes.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.notes.CommentCriteria;
import uz.personal.domain.notes.Comment;
import uz.personal.domain.notes.File;
import uz.personal.dto.GenericDto;
import uz.personal.dto.notes.CommentCreateDto;
import uz.personal.dto.notes.CommentDto;
import uz.personal.dto.notes.CommentUpdateDto;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.Notes.CommentMapper;
import uz.personal.repository.notes.ICommentRepository;
import uz.personal.repository.notes.IFileRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.notes.ICommentService;
import uz.personal.utils.BaseUtils;

import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import java.util.List;

@Service(value = "commentService")
@Slf4j
public class CommentService extends GenericCrudService<Comment, CommentDto, CommentCreateDto, CommentUpdateDto, CommentCriteria, ICommentRepository> implements ICommentService {
    private final GenericMapper genericMapper;
    private final CommentMapper commentMapper;

    @Autowired
    public CommentService(ICommentRepository repository, BaseUtils utils, IErrorRepository errorRepository, CommentMapper commentMapper, GenericMapper genericMapper) {
        super(repository, utils, errorRepository);
        this.commentMapper = commentMapper;
        this.genericMapper = genericMapper;
    }

    @Autowired
    IFileRepository fileRepository;

    @Override
    public ResponseEntity<DataDto<CommentDto>> get(Long id) {
        Comment comment = repository.find(CommentCriteria.childBuilder().selfId(id).build());
        validate(comment, id);
        return new ResponseEntity<>(new DataDto<>(commentMapper.toDto(comment)), HttpStatus.OK);
    }

    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public ResponseEntity<DataDto<List<CommentDto>>> getAll(CommentCriteria commentCriteria) {
        Long total = repository.getTotalCount(commentCriteria);
        return new ResponseEntity<>(new DataDto<>(commentMapper.toDto((repository.findAll(commentCriteria))), total), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull CommentCreateDto dto) {
        Comment comment = commentMapper.fromCreateDto(dto);
        baseValidation(comment);
        File file = fileRepository.find(dto.getFileId());
        if (file != null) {
            comment.setFile(file);
        } else {
            throw new ValidationException(ErrorCodes.OBJECT_NOT_FOUND_ID.code);
        }
        repository.save(comment);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(comment)), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataDto<CommentDto>> update(@NotNull CommentUpdateDto dto) {
        if (utils.isEmpty(dto.getId())) {
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "comment"));
        }

        Comment comment = repository.find(dto.getId());
        validate(comment, dto.getId());
        comment = commentMapper.fromUpdateDto(dto, comment);
        baseValidation(comment);
        comment = commentMapper.fromUpdateDto(dto, comment);
        baseValidation(comment);
        repository.update(comment);
        return get(comment.getId());
    }

    @Override
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        Comment comment = repository.find(id);
        validate(comment, id);
        repository.delete(comment);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull Comment entity) {
        if (utils.isEmpty(entity.getComment()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("comment", "Warehouse")));
    }

    @Override
    public void validate(@NotNull Comment entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Comment with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Comment", id)));
        }
    }
}
