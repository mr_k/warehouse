package uz.personal.service.notes.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.notes.FileCriteria;
import uz.personal.domain.notes.File;
import uz.personal.dto.GenericDto;
import uz.personal.dto.notes.FileCreateDto;
import uz.personal.dto.notes.FileDto;
import uz.personal.dto.notes.FileUpdateDto;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.Notes.FileMapper;
import uz.personal.repository.notes.IFileRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.notes.IFileService;
import uz.personal.utils.BaseUtils;
import javax.validation.constraints.NotNull;
import java.util.List;

@Service(value = "fileService")
@Slf4j
public class FileService extends GenericCrudService<File, FileDto, FileCreateDto, FileUpdateDto, FileCriteria, IFileRepository> implements IFileService {
    private final GenericMapper genericMapper;
    private final FileMapper fileMapper;

    @Autowired
    public FileService(IFileRepository repository, BaseUtils utils, IErrorRepository errorRepository, FileMapper fileMapper, GenericMapper genericMapper) {
        super(repository, utils, errorRepository);
        this.fileMapper = fileMapper;
        this.genericMapper = genericMapper;
    }

    @Override
    public ResponseEntity<DataDto<FileDto>> get(Long id) {
        File file = repository.find(FileCriteria.childBuilder().selfId(id).build());
        validate(file, id);
        return new ResponseEntity<>(new DataDto<>(fileMapper.toDto(file)), HttpStatus.OK);
    }

    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public ResponseEntity<DataDto<List<FileDto>>> getAll(FileCriteria fileCriteria) {
        Long total = repository.getTotalCount(fileCriteria);
        return new ResponseEntity<>(new DataDto<>(fileMapper.toDto((repository.findAll(fileCriteria))), total), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull FileCreateDto dto) {
        File file = fileMapper.fromCreateDto(dto);
        baseValidation(file);
        repository.save(file);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(file)), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataDto<FileDto>> update(@NotNull FileUpdateDto dto) {
        if (utils.isEmpty(dto.getId()))
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "files"));
        File file = repository.find(dto.getId());
        validate(file, dto.getId());
        file = fileMapper.fromUpdateDto(dto, file);
        baseValidation(file);
        file.setName(file.getName());
        file = fileMapper.fromUpdateDto(dto, file);
        baseValidation(file);
        repository.update(file);
        return get(file.getId());
    }

    @Override
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        File file = repository.find(id);
        validate(file, id);
        repository.delete(file);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull File entity) {
        if (utils.isEmpty(entity.getName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("name", "File")));
    }

    @Override
    public void validate(@NotNull File entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("File with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("File", id)));
        }
    }
}
