package uz.personal.service.notes;

import uz.personal.criteria.notes.FileCriteria;
import uz.personal.domain.notes.File;
import uz.personal.dto.notes.FileCreateDto;
import uz.personal.dto.notes.FileDto;
import uz.personal.dto.notes.FileUpdateDto;
import uz.personal.service.IGenericCrudService;

public interface IFileService  extends IGenericCrudService<File, FileDto, FileCreateDto, FileUpdateDto,Long, FileCriteria> {
}
