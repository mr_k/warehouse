package uz.personal.service.product;

import uz.personal.criteria.product.StoresCriteria;
import uz.personal.domain.product.Store;
import uz.personal.dto.product.StoreCreateDto;
import uz.personal.dto.product.StoreDto;
import uz.personal.dto.product.StoreUpdateDto;
import uz.personal.service.IGenericCrudService;

public interface IStoreService extends IGenericCrudService<Store, StoreDto, StoreCreateDto, StoreUpdateDto, Long, StoresCriteria> {
}
