package uz.personal.service.product;

import uz.personal.criteria.product.DisposeOfCriteria;
import uz.personal.domain.product.DisposeOf;
import uz.personal.dto.product.DisposeOfCreateDto;
import uz.personal.dto.product.DisposeOfDto;
import uz.personal.dto.product.DisposeOfUpdateDto;
import uz.personal.service.IGenericCrudService;

public interface IDisposeOfService extends IGenericCrudService<DisposeOf, DisposeOfDto, DisposeOfCreateDto, DisposeOfUpdateDto, Long, DisposeOfCriteria> {
}
