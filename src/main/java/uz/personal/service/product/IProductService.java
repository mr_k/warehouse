package uz.personal.service.product;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import uz.personal.criteria.product.ProductCriteria;
import uz.personal.domain.product.Product;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.ProductCreateDto;
import uz.personal.dto.product.ProductDto;
import uz.personal.dto.product.ProductUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.IGenericCrudService;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

public interface IProductService extends IGenericCrudService<Product, ProductDto, ProductCreateDto, ProductUpdateDto, Long, ProductCriteria> {
    ResponseEntity<DataDto<GenericDto>> creates(@NotNull MultipartFile multipartFile, @NotNull Long id) throws IOException;

    ResponseEntity<DataDto<ProductDto>> edit(ProductUpdateDto dto);

    ResponseEntity<DataDto<List<ProductDto>>> updates(List<ProductUpdateDto> productUpdateDtoList);
}
