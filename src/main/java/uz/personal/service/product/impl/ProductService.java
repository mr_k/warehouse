package uz.personal.service.product.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.personal.criteria.product.ProductCriteria;
import uz.personal.criteria.warehouse.WarehouseCriteria;
import uz.personal.domain.product.Product;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.ProductCreateDto;
import uz.personal.dto.product.ProductDto;
import uz.personal.dto.product.ProductUpdateDto;
import lombok.extern.slf4j.Slf4j;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.product.ProductMapper;
import uz.personal.repository.product.IProductRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.repository.warehouse.IWarehouseRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.product.IProductService;
import uz.personal.utils.BaseUtils;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service(value = "productService")
@Slf4j
public class ProductService extends GenericCrudService<Product, ProductDto, ProductCreateDto, ProductUpdateDto, ProductCriteria, IProductRepository> implements IProductService {


    private final GenericMapper genericMapper;
    private final ProductMapper productMapper;

    @Autowired
    public ProductService(IProductRepository repository, BaseUtils utils, IErrorRepository errorRepository, ProductMapper productMapper, GenericMapper genericMapper, IWarehouseRepository warehouseRepository) {
        super(repository, utils, errorRepository);
        this.productMapper = productMapper;
        this.genericMapper = genericMapper;
        this.warehouseRepository = warehouseRepository;
    }

    final
    IWarehouseRepository warehouseRepository;
    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public ResponseEntity<DataDto<ProductDto>> get(Long id) {
        Product product = repository.find(ProductCriteria.childBuilder().selfId(id).build());
        validate(product, id);
        return new ResponseEntity<>(new DataDto<>(productMapper.toDto(product)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<List<ProductDto>>> getAll(ProductCriteria productCriteria) {
        Long total = repository.getTotalCount(productCriteria);
        return new ResponseEntity<>(new DataDto<>(productMapper.toDto((repository.findAll(productCriteria))), total), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull ProductCreateDto dto) {
        Product product = productMapper.fromCreateDto(dto);
        baseValidation(product);
        Warehouse warehouse = warehouseRepository.find(dto.getWarehouseId());
        if (warehouse != null) {
            product.setWarehouse(warehouse);
        } else {
            throw new ValidationException(ErrorCodes.OBJECT_NOT_FOUND_ID.code);
        }
        repository.save(product);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(product)), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> creates(@NotNull MultipartFile multipartFile, @NotNull Long warehouseId) throws IOException {
        InputStream fis = multipartFile.getInputStream();
        XSSFWorkbook excelWorkbook = new XSSFWorkbook(fis);
        XSSFSheet excelSheet = excelWorkbook.getSheetAt(0);
        int rows = excelSheet.getPhysicalNumberOfRows();
        Warehouse warehouse = warehouseRepository.find(warehouseId);
        for (int i = 1; i < rows; i++) {
            Product product = new Product();
            for (int j = 0; j <= 9; j++) {
                XSSFCell cell = excelSheet.getRow(i).getCell(j);
                if (j == 0) {
                    product.setBarcode(cell.getStringCellValue());
                } else if (j == 1) {
                    product.setVendorCode(cell.getStringCellValue());
                } else if (j == 2) {
                    product.setName(cell.getStringCellValue());
                } else if (j == 3) {
                    product.setCategory(cell.getStringCellValue());
                } else if (j == 4) {
                    product.setSubcategory(cell.getStringCellValue());
                } else if (j == 5) {
                    product.setSize(Double.parseDouble(String.valueOf(cell.getNumericCellValue())));
                } else if (j == 6) {
                    product.setQuantity(Double.parseDouble(String.valueOf(cell.getNumericCellValue())));
                } else if (j == 7) {
                    product.setUzs_price(Double.parseDouble(String.valueOf(cell.getNumericCellValue())));
                } else if (j == 8) {
                    product.setDelivery_price_usd(Double.parseDouble(String.valueOf(cell.getNumericCellValue())));
                } else {
                    product.setDescription(cell.getStringCellValue());
                }
            }
            Product product1 = repository.find(ProductCriteria.childBuilder().venderCode(product.getVendorCode()).build());
            if (product1.getVendorCode().equals(product.getVendorCode())) {
                product1.setQuantity(product1.getQuantity() + product.getQuantity());
                repository.update(product1);
            } else {
                if (warehouse != null) {
                    product.setWarehouse(warehouse);
                } else {
                    throw new ValidationException(ErrorCodes.OBJECT_NOT_FOUND_ID.code);
                }
                repository.save(product);

            }

        }
        fis.close();
        return new ResponseEntity<>(new DataDto<>(), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataDto<ProductDto>> update(@NotNull ProductUpdateDto dto) {
        if (utils.isEmpty(dto.getId()))
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Product"));
        Product product = repository.find(dto.getId());
        validate(product, dto.getId());
        product = productMapper.fromUpdateDto(dto, product);
        baseValidation(product);
        repository.update(product);
        return get(product.getId());
    }

    @Override
    public ResponseEntity<DataDto<ProductDto>> edit(@NotNull ProductUpdateDto dto) {
        if (utils.isEmpty(dto.getId()))
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Product"));
        Product product = repository.find(dto.getId());
        validate(product, dto.getId());
        product.setQuantity(product.getQuantity() + dto.getQuantity());
        baseValidation(product);
        repository.update(product);
        return get(product.getId());
    }

    @Override
    public ResponseEntity<DataDto<List<ProductDto>>> updates(@NotNull List<ProductUpdateDto> productUpdateDtoList) {
        if (utils.isEmpty(productUpdateDtoList)) {
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Product"));
        }
        for (int i = 0; i < productUpdateDtoList.size(); i++) {
            Product product = repository.find(productUpdateDtoList.get(i).getId());
            validate(product, productUpdateDtoList.get(i).getId());
            if (productUpdateDtoList.get(i).getQuantity() < product.getQuantity()) {
                product.setQuantity(product.getQuantity() - productUpdateDtoList.get(i).getQuantity());
            }
            baseValidation(product);

            repository.update(product);
        }
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        Product product = repository.find(id);
        validate(product, id);
        repository.delete(product);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull Product entity) {
        if (utils.isEmpty(entity.getName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("name", "Product")));
    }

    @Override
    public void validate(@NotNull Product entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Product with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Product", id)));
        }
    }
}


