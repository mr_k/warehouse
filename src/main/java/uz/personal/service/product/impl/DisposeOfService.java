package uz.personal.service.product.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.product.DisposeOfCriteria;
import uz.personal.domain.product.Product;
import uz.personal.domain.product.DisposeOf;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.DisposeOfCreateDto;
import uz.personal.dto.product.DisposeOfDto;
import uz.personal.dto.product.DisposeOfUpdateDto;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.product.DisposeOfMapper;
import uz.personal.repository.product.IDisposeOfRepository;
import uz.personal.repository.product.IProductRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.product.IDisposeOfService;
import uz.personal.utils.BaseUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service(value = "disposeOfService")
@Slf4j
public class DisposeOfService extends GenericCrudService<DisposeOf, DisposeOfDto, DisposeOfCreateDto, DisposeOfUpdateDto, DisposeOfCriteria, IDisposeOfRepository> implements IDisposeOfService {
    private final GenericMapper genericMapper;
    private final DisposeOfMapper disposeOfMapper;

    @Autowired
    public DisposeOfService(IDisposeOfRepository repository, BaseUtils utils, IErrorRepository errorRepository, GenericMapper genericMapper, DisposeOfMapper disposeOfMapper) {
        super(repository, utils, errorRepository);
        this.genericMapper = genericMapper;
        this.disposeOfMapper = disposeOfMapper;
    }

    @Autowired
    IProductRepository productRepository;

    @Override
    public ResponseEntity<DataDto<DisposeOfDto>> get(Long id) {
        DisposeOf disposeOf = repository.find(DisposeOfCriteria.childBuilder().selfId(id).build());
        validate(disposeOf, id);
        return new ResponseEntity<>(new DataDto<>(disposeOfMapper.toDto(disposeOf)), HttpStatus.OK);
    }

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public ResponseEntity<DataDto<List<DisposeOfDto>>> getAll(DisposeOfCriteria criteria) {
        Long total = repository.getTotalCount(criteria);

        return new ResponseEntity<>(new DataDto<>(disposeOfMapper.toDto((repository.findAll(criteria))), total), HttpStatus.OK);

    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(DisposeOfCreateDto dto) {

        DisposeOf disposeOf = disposeOfMapper.fromCreateDto(dto);
        baseValidation(disposeOf);
        Product product = productRepository.find(dto.getProductId());
        if (product != null) {
            if (product.getQuantity() >= dto.getQuantity()) {
                product.setQuantity(product.getQuantity() - dto.getQuantity());
                productRepository.update(product);
            } else {
                throw new ValidationException(ErrorCodes.PRODUCT_IS_NOT_ENOUGH.code);
            }
        } else {
            throw new ValidationException(ErrorCodes.OBJECT_NOT_FOUND_ID.code);
        }
        disposeOf.setProduct(product);
        repository.save(disposeOf);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(disposeOf)), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataDto<DisposeOfDto>> update(@NotNull DisposeOfUpdateDto dto) {
        if (utils.isEmpty(dto.getId()))
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "disposeOf"));
        DisposeOf disposeOf = repository.find(dto.getId());

        validate(disposeOf, dto.getId());

        disposeOf = disposeOfMapper.fromUpdateDto(dto, disposeOf);

        baseValidation(disposeOf);

        repository.update(disposeOf);

        return get(disposeOf.getId());
    }

    @Override
    public ResponseEntity<DataDto<Boolean>> delete(Long id) {
        DisposeOf disposeOf = repository.find(id);
        validate(disposeOf, id);
        repository.delete(disposeOf);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(DisposeOf entity) {
        super.baseValidation(entity);
        if (utils.isEmpty(entity.getQuantity()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("quantity", "WriteOff")));
    }

    @Override
    public void validate(DisposeOf entity, Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("WriteOff with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("WriteOff", id)));
        }
    }
}
