package uz.personal.service.product.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.product.StoresCriteria;
import uz.personal.domain.product.Store;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.*;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.product.StoreMapper;
import uz.personal.repository.product.IStoreRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.product.IStoreService;
import uz.personal.utils.BaseUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service(value = "storeService")
@Slf4j

public class StoreService extends GenericCrudService<Store, StoreDto, StoreCreateDto, StoreUpdateDto, StoresCriteria, IStoreRepository> implements IStoreService {
    private final GenericMapper genericMapper;
    private final StoreMapper storeMapper;
   @Autowired
    public StoreService(IStoreRepository repository, BaseUtils utils, IErrorRepository errorRepository, StoreMapper storeMapper, GenericMapper genericMapper) {
        super(repository, utils, errorRepository);
        this.storeMapper = storeMapper;
        this.genericMapper = genericMapper;
    }

    @Override
    public ResponseEntity<DataDto<StoreDto>> get(Long id) {
        Store store = repository.find(StoresCriteria.childBuilder().selfId(id).build());
        validate(store, id);
        return new ResponseEntity<>(new DataDto<>(storeMapper.toDto(store)), HttpStatus.OK);
    }

    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public ResponseEntity<DataDto<List<StoreDto>>> getAll(StoresCriteria storesCriteria) {
        Long total = repository.getTotalCount(storesCriteria);
        return new ResponseEntity<>(new DataDto<>(storeMapper.toDto((repository.findAll(storesCriteria))), total), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull StoreCreateDto dto) {
        Store store = storeMapper.fromCreateDto(dto);
        baseValidation(store);
        repository.save(store);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(store)), HttpStatus.CREATED);
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PERMISSION_UPDATE)")
        public ResponseEntity<DataDto<StoreDto>> update(@NotNull StoreUpdateDto dto) {
            if (utils.isEmpty(dto.getId()))
                throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Stores"));
            Store store = repository.find(dto.getId());
            validate(store, dto.getId());
            store = storeMapper.fromUpdateDto(dto, store);
            baseValidation(store);
            store.setName(store.getName());
            store = storeMapper.fromUpdateDto(dto, store);
            baseValidation(store);
            repository.update(store);
            return get(store.getId());
        }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PERMISSION_DELETE)")
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        Store store = repository.find(id);
        validate(store, id);
        repository.delete(store);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull Store entity) {
        if (utils.isEmpty(entity.getName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("name", "Stores")));
    }

    @Override
    public void validate(@NotNull Store entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Stores with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Stores", id)));
        }
    }
}
