package uz.personal.service.client;

import uz.personal.criteria.client.ClientCriteria;
import uz.personal.domain.client.Client;
import uz.personal.dto.client.ClientCreateDto;
import uz.personal.dto.client.ClientDto;
import uz.personal.dto.client.ClientUpdateDto;
import uz.personal.service.IGenericCrudService;
public interface IClientService extends IGenericCrudService<Client, ClientDto, ClientCreateDto, ClientUpdateDto, Long, ClientCriteria> {
}
