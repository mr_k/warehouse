package uz.personal.service.client;

import uz.personal.criteria.client.CompanyCriteria;
import uz.personal.domain.client.Company;
import uz.personal.dto.client.CompanyCreateDto;
import uz.personal.dto.client.CompanyDto;
import uz.personal.dto.client.CompanyUpdateDto;
import uz.personal.service.IGenericCrudService;

public interface ICompanyService extends IGenericCrudService<Company, CompanyDto, CompanyCreateDto, CompanyUpdateDto,Long, CompanyCriteria> {
}
