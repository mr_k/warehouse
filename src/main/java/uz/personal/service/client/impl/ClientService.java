package uz.personal.service.client.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.client.ClientCriteria;
import uz.personal.domain.client.Client;
import uz.personal.dto.GenericDto;
import uz.personal.dto.client.ClientCreateDto;
import uz.personal.dto.client.ClientDto;
import uz.personal.dto.client.ClientUpdateDto;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.client.ClientMapper;
import uz.personal.repository.client.IClientRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.client.IClientService;
import uz.personal.utils.BaseUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Slf4j
public class ClientService extends GenericCrudService<Client, ClientDto, ClientCreateDto, ClientUpdateDto, ClientCriteria, IClientRepository> implements IClientService {
    private final Log logger = LogFactory.getLog(getClass());
    private final ClientMapper clientMapper;
    private final GenericMapper genericMapper;

    @Autowired
    public ClientService(IClientRepository repository, BaseUtils utils, IErrorRepository errorRepository, ClientMapper clientMapper, GenericMapper genericMapper) {
        super(repository, utils, errorRepository);
        this.clientMapper = clientMapper;
        this.genericMapper = genericMapper;
    }

    @Override
    public ResponseEntity<DataDto<ClientDto>> get(Long id) {
        Client client = repository.find(ClientCriteria.childBuilder().selfId(id).build());
        validate(client, id);
        return new ResponseEntity<>(new DataDto<>(clientMapper.toDto(client)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<List<ClientDto>>> getAll(ClientCriteria clientCriteria) {
        Long total = repository.getTotalCount(clientCriteria);
        return new ResponseEntity<>(new DataDto<>(clientMapper.toDto((repository.findAll(clientCriteria))), total), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull ClientCreateDto dto) {
        Client client = clientMapper.fromCreateDto(dto);
        baseValidation(client);
            repository.save(client);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(client)), HttpStatus.CREATED);
    }

    public ResponseEntity<DataDto<ClientDto>> update(@NotNull ClientUpdateDto dto) {
        if (utils.isEmpty(dto.getId()))
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Clients"));
        Client client = repository.find(dto.getId());
        validate(client, dto.getId());
        client = clientMapper.fromUpdateDto(dto, client);
        baseValidation(client);
        repository.update(client);
        return get(client.getId());
    }

    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        Client client = repository.find(id);
        validate(client, id);
        repository.delete(client);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull Client entity) {
        if (utils.isEmpty(entity.getClientName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("clientName", "Client")));
    }

    @Override
    public void validate(@NotNull Client entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Client with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Client", id)));
        }
    }

}
