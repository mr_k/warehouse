package uz.personal.service.client.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.client.CompanyCriteria;
import uz.personal.domain.client.Company;
import uz.personal.dto.GenericDto;
import uz.personal.dto.client.*;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.client.CompanyMapper;
import uz.personal.repository.client.ICompanyRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.client.ICompanyService;
import uz.personal.utils.BaseUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
@Service
@Slf4j
public class CompanyService extends GenericCrudService<Company, CompanyDto, CompanyCreateDto, CompanyUpdateDto, CompanyCriteria, ICompanyRepository> implements ICompanyService {
    private final Log logger = LogFactory.getLog(getClass());
    private final CompanyMapper companyMapper;
    private final GenericMapper genericMapper;

    @Autowired
    public CompanyService(ICompanyRepository repository, BaseUtils utils, IErrorRepository errorRepository, CompanyMapper companyMapper, GenericMapper genericMapper) {
        super(repository, utils, errorRepository);
        this.companyMapper = companyMapper;
        this.genericMapper = genericMapper;
    }

    @Override
    public ResponseEntity<DataDto<CompanyDto>> get(Long id) {
        Company company = repository.find(CompanyCriteria.childBuilder().selfId(id).build());
        validate(company, id);
        return new ResponseEntity<>(new DataDto<>(companyMapper.toDto(company)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<List<CompanyDto>>> getAll(CompanyCriteria companyCriteria) {
        Long total = repository.getTotalCount(companyCriteria);
        return new ResponseEntity<>(new DataDto<>(companyMapper.toDto((repository.findAll(companyCriteria))), total), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull CompanyCreateDto dto) {
        Company company = companyMapper.fromCreateDto(dto);
        baseValidation(company);
        repository.save(company);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(company)), HttpStatus.CREATED);
    }

    public ResponseEntity<DataDto<CompanyDto>> update(@NotNull CompanyUpdateDto dto) {
        if (utils.isEmpty(dto.getId()))
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "kompany"));
        Company company = repository.find(dto.getId());
        validate(company, dto.getId());
        company = companyMapper.fromUpdateDto(dto, company);
        baseValidation(company);
        repository.update(company);
        return get(company.getId());
    }

    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        Company company = repository.find(id);
        validate(company, id);
        repository.delete(company);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull Company entity) {
        if (utils.isEmpty(entity.getCompanyName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("companyName", "Company")));
    }

    @Override
    public void validate(@NotNull Company entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Company with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Company", id)));
        }
    }

}

