package uz.personal.service.warehouse.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.warehouse.WarehouseCriteria;
import uz.personal.domain.product.Store;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.dto.GenericDto;
import uz.personal.dto.warehouse.WarehouseCreateDto;
import uz.personal.dto.warehouse.WarehouseDto;
import uz.personal.dto.warehouse.WarehouseUpdateDto;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.warehouse.WarehouseMapper;
import uz.personal.repository.product.IStoreRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.repository.warehouse.IWarehouseRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.product.impl.StoreService;
import uz.personal.service.warehouse.IWarehouseService;
import uz.personal.utils.BaseUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service(value = "warehouseService")
@Slf4j
public class WarehouseService extends GenericCrudService<Warehouse, WarehouseDto, WarehouseCreateDto, WarehouseUpdateDto, WarehouseCriteria, IWarehouseRepository> implements IWarehouseService {
    private final GenericMapper genericMapper;
    private final WarehouseMapper warehouseMapper;

    @Autowired
    public WarehouseService(IWarehouseRepository repository, BaseUtils utils, IErrorRepository errorRepository, WarehouseMapper warehouseMapper, GenericMapper genericMapper) {
        super(repository, utils, errorRepository);
        this.warehouseMapper = warehouseMapper;
        this.genericMapper = genericMapper;
    }

    @Autowired
    IStoreRepository storeRepository;

    @Override
    public ResponseEntity<DataDto<WarehouseDto>> get(Long id) {
        Warehouse warehouse = repository.find(WarehouseCriteria.childBuilder().selfId(id).build());
        validate(warehouse, id);
        return new ResponseEntity<>(new DataDto<>(warehouseMapper.toDto(warehouse)), HttpStatus.OK);
    }

    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public ResponseEntity<DataDto<List<WarehouseDto>>> getAll(WarehouseCriteria warehouseCriteria) {
        Long total = repository.getTotalCount(warehouseCriteria);
        return new ResponseEntity<>(new DataDto<>(warehouseMapper.toDto((repository.findAll(warehouseCriteria))), total), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull WarehouseCreateDto dto) {
        Warehouse warehouse = warehouseMapper.fromCreateDto(dto);
        baseValidation(warehouse);
        Store store = storeRepository.find(dto.getStoreId());
        if (store != null) {
            warehouse.setStore(store);
        } else {
            throw new ValidationException(ErrorCodes.OBJECT_NOT_FOUND_ID.code);
        }
        repository.save(warehouse);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(warehouse)), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataDto<WarehouseDto>> update(@NotNull WarehouseUpdateDto dto) {
        if (utils.isEmpty(dto.getId())) {
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Stores"));
        }

        Warehouse warehouse = repository.find(dto.getId());
        validate(warehouse, dto.getId());
        warehouse = warehouseMapper.fromUpdateDto(dto, warehouse);
        baseValidation(warehouse);
        warehouse.setName(warehouse.getName());
        warehouse = warehouseMapper.fromUpdateDto(dto, warehouse);
        baseValidation(warehouse);
        repository.update(warehouse);
        return get(warehouse.getId());
    }

    @Override
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        Warehouse warehouse = repository.find(id);
        validate(warehouse, id);
        repository.delete(warehouse);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull Warehouse entity) {
        if (utils.isEmpty(entity.getName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("name", "Warehouse")));
    }

    @Override
    public void validate(@NotNull Warehouse entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Warehouse with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Warehouse", id)));
        }
    }
    }

