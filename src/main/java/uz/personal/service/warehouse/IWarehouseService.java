package uz.personal.service.warehouse;

import org.springframework.http.ResponseEntity;
import uz.personal.criteria.warehouse.WarehouseCriteria;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.dto.warehouse.WarehouseCreateDto;
import uz.personal.dto.warehouse.WarehouseDto;
import uz.personal.dto.warehouse.WarehouseUpdateDto;
import uz.personal.response.DataDto;
import uz.personal.service.IGenericCrudService;

import java.util.List;

public interface IWarehouseService extends IGenericCrudService<Warehouse, WarehouseDto, WarehouseCreateDto, WarehouseUpdateDto, Long, WarehouseCriteria> {
//    ResponseEntity<DataDto<List<WarehouseDto>>> getByStoreId(Long storeId);
}
