package uz.personal.service.inventory;

import uz.personal.criteria.inventory.InventoryCriteria;
import uz.personal.domain.inventory.Inventory;
import uz.personal.dto.inventory.InventoryCreateDto;
import uz.personal.dto.inventory.InventoryDto;
import uz.personal.dto.inventory.InventoryUpdateDto;
import uz.personal.service.IGenericCrudService;

public interface IInventoryService extends IGenericCrudService<Inventory, InventoryDto, InventoryCreateDto, InventoryUpdateDto, Long, InventoryCriteria> {
}
