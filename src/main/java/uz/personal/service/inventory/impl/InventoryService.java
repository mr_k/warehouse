package uz.personal.service.inventory.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.personal.criteria.inventory.InventoryCriteria;
import uz.personal.domain.inventory.Inventory;
import uz.personal.domain.product.Product;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.dto.GenericDto;
import uz.personal.dto.inventory.InventoryCreateDto;
import uz.personal.dto.inventory.InventoryDto;
import uz.personal.dto.inventory.InventoryUpdateDto;
import uz.personal.enums.ErrorCodes;
import uz.personal.exception.IdRequiredException;
import uz.personal.exception.ValidationException;
import uz.personal.mapper.GenericMapper;
import uz.personal.mapper.inventory.InventoryMapper;
import uz.personal.repository.inventory.IInventoryRepository;
import uz.personal.repository.product.IProductRepository;
import uz.personal.repository.setting.IErrorRepository;
import uz.personal.repository.warehouse.IWarehouseRepository;
import uz.personal.response.DataDto;
import uz.personal.service.GenericCrudService;
import uz.personal.service.inventory.IInventoryService;
import uz.personal.utils.BaseUtils;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service(value = "inventoryService")
@Slf4j
class InventoryService extends GenericCrudService<Inventory, InventoryDto, InventoryCreateDto, InventoryUpdateDto, InventoryCriteria, IInventoryRepository> implements IInventoryService {
    private final Log logger = LogFactory.getLog(getClass());
    private final InventoryMapper inventoryMapper;
    private final GenericMapper genericMapper;
    private final IProductRepository productRepository;

    @Autowired
    public InventoryService(IInventoryRepository repository, BaseUtils utils, IErrorRepository errorRepository, InventoryMapper inventoryMapper, GenericMapper genericMapper, IProductRepository productRepository) {
        super(repository, utils, errorRepository);
        this.inventoryMapper = inventoryMapper;
        this.genericMapper = genericMapper;
        this.productRepository = productRepository;
    }

    @Autowired
    IWarehouseRepository warehouseRepository;

    @Override
    public ResponseEntity<DataDto<InventoryDto>> get(Long id) {
        Inventory inventory = repository.find(InventoryCriteria.childBuilder().selfId(id).build());
        validate(inventory, id);
        return new ResponseEntity<>(new DataDto<>(inventoryMapper.toDto(inventory)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DataDto<List<InventoryDto>>> getAll(InventoryCriteria inventoryCriteria) {
        {
            Long total = repository.getTotalCount(inventoryCriteria);
            return new ResponseEntity<>(new DataDto<>(inventoryMapper.toDto(repository.findAll(inventoryCriteria)), total), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull InventoryCreateDto dto) {
        Inventory inventory = inventoryMapper.fromCreateDto(dto);
        baseValidation(inventory);
        Product product = productRepository.find(dto.getProductId());
        if (product != null) {
            if (dto.isSign()) {
                product.setQuantity(product.getQuantity() + dto.getQuantity());
            } else {
                if (dto.getQuantity() <= product.getQuantity()) {
                    product.setQuantity(product.getQuantity() - dto.getQuantity());
                } else {
                    throw new ValidationException(ErrorCodes.PRODUCT_IS_NOT_ENOUGH.code);
                }
            }
        } else {
            throw new ValidationException(ErrorCodes.OBJECT_NOT_FOUND_ID.code);
        }

        productRepository.update(product);
        inventory.setProduct(product);
        repository.save(inventory);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(inventory)), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DataDto<InventoryDto>> update(@NotNull InventoryUpdateDto dto) {
//        Inventory inventory = repository.find(InventoryCriteria.childBuilder().warehouseId(dto.getWarehouseId()).productId(dto.getProductId()).build());
        if (utils.isEmpty(dto.getId())) {
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Inventory"));
        }
        Inventory inventory = repository.find(dto.getId());
        baseValidation(inventory);
        repository.update(inventory);

        return get(inventory.getId());
    }

    @Override
    public ResponseEntity<DataDto<Boolean>> delete(Long id) {
        Inventory inventory = repository.find(id);
        validate(inventory, id);
        repository.delete(inventory);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull Inventory entity) {
        if (utils.isEmpty(entity.getDescription()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("quantity", "Inventory")));
    }

    @Override
    public void validate(@NotNull Inventory entity, Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Inventory with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Inventory", id)));
        }
    }

//    public void updateValidate(@NotNull InventoryUpdateDto dto) {
//        if ((utils.isEmpty(dto.getStoreId()))) {
//            logger.error(String.format("Inventory with storeId '%s' not found", dto.getStoreId()));
//            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Inventory", storeId)));
//        }
//        if ((utils.isEmpty(dto.getProductId()))) {
//            logger.error(String.format("Inventory with productId '%s' not found", dto.getProductId()));
//            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Inventory", productId)));
//        }
//    }
}
