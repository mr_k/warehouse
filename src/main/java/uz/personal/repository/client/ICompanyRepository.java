package uz.personal.repository.client;

import uz.personal.criteria.client.CompanyCriteria;
import uz.personal.domain.client.Company;
import uz.personal.repository.IGenericCrudRepository;

public interface ICompanyRepository extends IGenericCrudRepository<Company, CompanyCriteria> {
}
