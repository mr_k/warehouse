package uz.personal.repository.client;

import uz.personal.criteria.client.ClientCriteria;
import uz.personal.domain.client.Client;
import uz.personal.repository.IGenericCrudRepository;

public interface IClientRepository extends IGenericCrudRepository<Client, ClientCriteria> {
}
