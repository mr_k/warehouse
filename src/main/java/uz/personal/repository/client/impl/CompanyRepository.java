package uz.personal.repository.client.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import uz.personal.criteria.client.CompanyCriteria;
import uz.personal.domain.client.Company;
import uz.personal.repository.GenericDao;
import uz.personal.repository.client.ICompanyRepository;

import java.util.List;
import java.util.Map;

@Repository
public class CompanyRepository extends GenericDao<Company, CompanyCriteria> implements ICompanyRepository {
    protected final Log logger = LogFactory.getLog(getClass());
    @Override
    protected void defineCriteriaOnQuerying(CompanyCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        super.defineCriteriaOnQuerying(criteria, whereCause, params, queryBuilder);
        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add("t.id = :selfId");
            params.put("selfId", criteria.getSelfId());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
}
