package uz.personal.repository.inventory.impl;

import org.springframework.stereotype.Repository;
import uz.personal.criteria.inventory.InventoryCriteria;
import uz.personal.domain.inventory.Inventory;
import uz.personal.domain.product.Product;
import uz.personal.repository.GenericDao;
import uz.personal.repository.inventory.IInventoryRepository;

import java.util.List;
import java.util.Map;

@Repository
public class InventoryRepository extends GenericDao<Inventory, InventoryCriteria> implements IInventoryRepository {
    @Override
    protected void defineCriteriaOnQuerying(InventoryCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        super.defineCriteriaOnQuerying(criteria, whereCause, params, queryBuilder);
    }

    }
