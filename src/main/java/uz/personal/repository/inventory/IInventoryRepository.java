package uz.personal.repository.inventory;

import uz.personal.criteria.inventory.InventoryCriteria;
import uz.personal.domain.inventory.Inventory;
import uz.personal.repository.IGenericCrudRepository;
public interface IInventoryRepository extends IGenericCrudRepository<Inventory, InventoryCriteria> {
}
