package uz.personal.repository.notes;

import uz.personal.criteria.notes.CommentCriteria;
import uz.personal.domain.notes.Comment;
import uz.personal.repository.IGenericCrudRepository;

public interface ICommentRepository extends IGenericCrudRepository<Comment, CommentCriteria> {
}
