package uz.personal.repository.notes;

import uz.personal.criteria.notes.FileCriteria;
import uz.personal.domain.notes.File;
import uz.personal.repository.IGenericCrudRepository;

public interface IFileRepository extends IGenericCrudRepository<File, FileCriteria> {
}
