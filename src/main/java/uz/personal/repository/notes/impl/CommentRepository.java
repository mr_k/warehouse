package uz.personal.repository.notes.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import uz.personal.criteria.notes.CommentCriteria;
import uz.personal.domain.notes.Comment;
import uz.personal.repository.GenericDao;
import uz.personal.repository.notes.ICommentRepository;

import java.util.List;
import java.util.Map;

@Repository
public class CommentRepository extends GenericDao<Comment, CommentCriteria>  implements ICommentRepository {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    protected void defineCriteriaOnQuerying(CommentCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add("t.id = :selfId");
            params.put("selfId", criteria.getSelfId());
        }
        if (!utils.isEmpty(criteria.getFileId())) {
            whereCause.add("t.file.id = :fileId");
            params.put("fileId", criteria.getFileId());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
}
