package uz.personal.repository.notes.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import uz.personal.criteria.notes.FileCriteria;
import uz.personal.domain.notes.File;
import uz.personal.repository.GenericDao;
import uz.personal.repository.notes.IFileRepository;

import java.util.List;
import java.util.Map;

@Repository
public class FileRepository extends GenericDao<File, FileCriteria> implements IFileRepository {
    protected final Log logger = LogFactory.getLog(getClass());
    @Override
    protected void defineCriteriaOnQuerying(FileCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add("t.id = :selfId");
            params.put("selfId", criteria.getSelfId());
        }
        if (!utils.isEmpty(criteria.getName())) {
            whereCause.add("lower (t.name) like lower(:name)");
            params.put("name", "%" + criteria.getName() + "%");
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
}
