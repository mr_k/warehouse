package uz.personal.repository.warehouse.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import uz.personal.criteria.warehouse.WarehouseCriteria;
import uz.personal.domain.auth._User;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.repository.GenericDao;
import uz.personal.repository.warehouse.IWarehouseRepository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Map;

@Repository
public class WarehouseRepository extends GenericDao<Warehouse, WarehouseCriteria> implements IWarehouseRepository {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    protected void defineCriteriaOnQuerying(WarehouseCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add("t.id = :selfId");
            params.put("selfId", criteria.getSelfId());
        }
        if (!utils.isEmpty(criteria.getName())) {
            whereCause.add("lower (t.name) like lower(:name)");
            params.put("name", "%" + criteria.getName() + "%");
        }
        if (!utils.isEmpty(criteria.getStoreId())) {
            whereCause.add("t.store.id = :storeId");
            params.put("storeId", criteria.getStoreId());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
}
