package uz.personal.repository.warehouse;

import uz.personal.criteria.warehouse.WarehouseCriteria;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.repository.IGenericCrudRepository;

public interface IWarehouseRepository  extends IGenericCrudRepository<Warehouse, WarehouseCriteria> {
}
