package uz.personal.repository.product.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import uz.personal.criteria.product.StoresCriteria;
import uz.personal.domain.product.Store;
import uz.personal.repository.GenericDao;
import uz.personal.repository.product.IStoreRepository;

import java.util.List;
import java.util.Map;

@Repository
public class StoreRepository extends GenericDao<Store, StoresCriteria>  implements IStoreRepository {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    protected void defineCriteriaOnQuerying(StoresCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        super.defineCriteriaOnQuerying(criteria, whereCause, params, queryBuilder);
        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add("t.id = :selfId");
            params.put("selfId", criteria.getSelfId());
        }
        if (!utils.isEmpty(criteria.getName())) {
            whereCause.add("t.name = :name");
            params.put("name", criteria.getName());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
    @Override
    public Store update(Store entity) {
        return super.update(entity);
    }

    @Override
    public void delete(Store entity) {
        super.delete(entity);
    }

}

