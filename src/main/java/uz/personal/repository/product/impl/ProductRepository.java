package uz.personal.repository.product.impl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import uz.personal.criteria.product.ProductCriteria;
import uz.personal.domain.product.Product;
import uz.personal.repository.GenericDao;
import uz.personal.repository.product.IProductRepository;
import java.util.List;
import java.util.Map;

@Repository
public class ProductRepository extends GenericDao<Product, ProductCriteria> implements IProductRepository {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    protected void defineCriteriaOnQuerying(ProductCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        super.defineCriteriaOnQuerying(criteria, whereCause, params, queryBuilder);
        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add("t.id = :selfId");
            params.put("selfId", criteria.getSelfId());
        }
        if (!utils.isEmpty(criteria.getName())) {
            whereCause.add("lower (t.name) like lower(:name)");
            params.put("name", "%" + criteria.getName() + "%");
        }
        if (!utils.isEmpty(criteria.getBarcode())) {
            whereCause.add("lower (t.barcode) like lower(:barcode)");
            params.put("barcode", "%" + criteria.getBarcode() + "%");
        }
        if (!utils.isEmpty(criteria.getVendorCode())) {
            whereCause.add("lower (t.vendorCode) like lower(:vendorCode)");
            params.put("vendorCode", "%" + criteria.getVendorCode() + "%");
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }

    @Override
    public Product update(Product entity) {
        return super.update(entity);
    }

    @Override
    public void delete(Product entity) {
        super.delete(entity);
    }
}
