package uz.personal.repository.product.impl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import uz.personal.criteria.product.DisposeOfCriteria;
import uz.personal.domain.product.DisposeOf;
import uz.personal.repository.GenericDao;
import uz.personal.repository.product.IDisposeOfRepository;
import java.util.List;
import java.util.Map;
@Repository
public class DisposeOfRepository extends GenericDao<DisposeOf, DisposeOfCriteria> implements IDisposeOfRepository {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    protected void defineCriteriaOnQuerying(DisposeOfCriteria criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        super.defineCriteriaOnQuerying(criteria, whereCause, params, queryBuilder);
        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add("t.id = :selfId");
            params.put("selfId", criteria.getSelfId());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
    @Override
    public DisposeOf update(DisposeOf entity) {
        return super.update(entity);
    }

    @Override
    public void delete(DisposeOf entity) {
        super.delete(entity);
    }
}
