package uz.personal.repository.product;
import uz.personal.criteria.product.ProductCriteria;
import uz.personal.domain.product.Product;
import uz.personal.repository.IGenericCrudRepository;

public interface IProductRepository extends IGenericCrudRepository<Product, ProductCriteria> {
}
