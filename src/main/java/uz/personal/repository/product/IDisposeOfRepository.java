package uz.personal.repository.product;

import uz.personal.criteria.product.DisposeOfCriteria;
import uz.personal.domain.product.DisposeOf;
import uz.personal.repository.IGenericCrudRepository;

public interface IDisposeOfRepository extends IGenericCrudRepository<DisposeOf, DisposeOfCriteria> {
}
