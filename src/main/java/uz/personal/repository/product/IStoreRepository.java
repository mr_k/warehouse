package uz.personal.repository.product;

import uz.personal.criteria.product.StoresCriteria;
import uz.personal.domain.product.Store;
import uz.personal.repository.IGenericCrudRepository;

public interface IStoreRepository extends IGenericCrudRepository<Store, StoresCriteria> {
}
