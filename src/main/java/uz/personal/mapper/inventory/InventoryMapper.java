package uz.personal.mapper.inventory;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.inventory.Inventory;
import uz.personal.dto.inventory.InventoryCreateDto;
import uz.personal.dto.inventory.InventoryDto;
import uz.personal.dto.inventory.InventoryUpdateDto;
import uz.personal.mapper.BaseMapper;
import uz.personal.mapper.product.ProductMapper;
import uz.personal.mapper.product.StoreMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {ProductMapper.class, StoreMapper.class})
@Component
public interface InventoryMapper extends BaseMapper<Inventory, InventoryDto, InventoryCreateDto, InventoryUpdateDto> {
}
