package uz.personal.mapper.Notes;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.notes.File;
import uz.personal.dto.notes.FileCreateDto;
import uz.personal.dto.notes.FileDto;
import uz.personal.dto.notes.FileUpdateDto;
import uz.personal.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface FileMapper extends BaseMapper<File, FileDto, FileCreateDto, FileUpdateDto> {
}
