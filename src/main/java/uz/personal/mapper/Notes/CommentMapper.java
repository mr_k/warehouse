package uz.personal.mapper.Notes;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.notes.Comment;
import uz.personal.dto.notes.CommentCreateDto;
import uz.personal.dto.notes.CommentDto;
import uz.personal.dto.notes.CommentUpdateDto;
import uz.personal.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {FileMapper.class})
@Component
public interface CommentMapper extends BaseMapper<Comment, CommentDto, CommentCreateDto, CommentUpdateDto> {
}
