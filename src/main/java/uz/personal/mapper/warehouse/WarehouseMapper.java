package uz.personal.mapper.warehouse;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.dto.warehouse.WarehouseCreateDto;
import uz.personal.dto.warehouse.WarehouseDto;
import uz.personal.dto.warehouse.WarehouseUpdateDto;
import uz.personal.mapper.BaseMapper;
import uz.personal.mapper.product.StoreMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {StoreMapper.class})
@Component
public interface WarehouseMapper  extends BaseMapper<Warehouse, WarehouseDto, WarehouseCreateDto, WarehouseUpdateDto> {
}
