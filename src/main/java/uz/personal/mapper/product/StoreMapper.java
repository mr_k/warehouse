package uz.personal.mapper.product;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.product.Store;
import uz.personal.dto.product.StoreCreateDto;
import uz.personal.dto.product.StoreDto;
import uz.personal.dto.product.StoreUpdateDto;
import uz.personal.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface StoreMapper extends BaseMapper<Store, StoreDto, StoreCreateDto, StoreUpdateDto> {
}
