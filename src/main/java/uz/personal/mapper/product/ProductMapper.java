package uz.personal.mapper.product;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.product.Product;
import uz.personal.dto.product.ProductCreateDto;
import uz.personal.dto.product.ProductDto;
import uz.personal.dto.product.ProductUpdateDto;
import uz.personal.mapper.BaseMapper;
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface ProductMapper extends BaseMapper<Product, ProductDto, ProductCreateDto, ProductUpdateDto> {
}
