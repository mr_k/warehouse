package uz.personal.mapper.product;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.product.DisposeOf;
import uz.personal.dto.product.DisposeOfCreateDto;
import uz.personal.dto.product.DisposeOfDto;
import uz.personal.dto.product.DisposeOfUpdateDto;
import uz.personal.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface DisposeOfMapper extends BaseMapper<DisposeOf, DisposeOfDto, DisposeOfCreateDto, DisposeOfUpdateDto> {
}
