package uz.personal.mapper.client;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.client.Client;
import uz.personal.dto.client.ClientCreateDto;
import uz.personal.dto.client.ClientDto;
import uz.personal.dto.client.ClientUpdateDto;
import uz.personal.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface ClientMapper extends BaseMapper<Client, ClientDto, ClientCreateDto, ClientUpdateDto> {
}
