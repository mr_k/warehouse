package uz.personal.mapper.client;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import uz.personal.domain.client.Company;
import uz.personal.dto.client.CompanyCreateDto;
import uz.personal.dto.client.CompanyDto;
import uz.personal.dto.client.CompanyUpdateDto;
import uz.personal.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface CompanyMapper extends BaseMapper<Company, CompanyDto, CompanyCreateDto, CompanyUpdateDto> {
}
