package uz.personal.criteria.inventory;

import lombok.*;
import uz.personal.criteria.GenericCriteria;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InventoryCriteria  extends GenericCriteria {
    private Long quantity;
    private Long warehouseId;
    private Long productId;
    @Builder(builderMethodName = "childBuilder")
    public InventoryCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, Long quantity,Long warehouseId,Long productId) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.quantity = quantity;
        this.warehouseId=warehouseId;
        this.productId=productId;
    }
}
