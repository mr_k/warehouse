package uz.personal.criteria.warehouse;

import lombok.*;
import uz.personal.criteria.GenericCriteria;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseCriteria extends GenericCriteria {
    private String name;
    private String location;
    private Long storeId;
    @Builder(builderMethodName = "childBuilder")

    public WarehouseCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String name, String location, Long storeId) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.name = name;
        this.location = location;
        this.storeId = storeId;
    }
}
