package uz.personal.criteria.notes;

import lombok.*;
import uz.personal.criteria.GenericCriteria;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class FileCriteria extends GenericCriteria {
private String name;
    @Builder(builderMethodName = "childBuilder")

    public FileCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String name) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.name = name;
    }
}
