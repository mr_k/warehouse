package uz.personal.criteria.notes;

import lombok.*;
import uz.personal.criteria.GenericCriteria;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CommentCriteria extends GenericCriteria {
    private String comment;
    private Long fileId;
    @Builder(builderMethodName = "childBuilder")

    public CommentCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String comment, Long fileId) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.comment = comment;
        this.fileId = fileId;
    }
}
