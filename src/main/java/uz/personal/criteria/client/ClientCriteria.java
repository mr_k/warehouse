package uz.personal.criteria.client;

import lombok.*;
import uz.personal.criteria.GenericCriteria;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientCriteria extends GenericCriteria {
    private String clientName;
    private String phoneNumber;
    private String email;
    private String address;
    private String interest;
    private String discountCard;
    private Double discountOnService;
    private String note;
    private String tags;

    @Builder(builderMethodName = "childBuilder")
    public ClientCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String clientName, String phoneNumber, String email, String address, String interest, String discountCard, Double discountOnService, String note, String tags) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.clientName = clientName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.interest = interest;
        this.discountCard = discountCard;
        this.discountOnService = discountOnService;
        this.note = note;
        this.tags = tags;
    }
}
