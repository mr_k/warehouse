package uz.personal.criteria.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.criteria.GenericCriteria;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DisposeOfCriteria extends GenericCriteria {
    private String description;
    private Double quantity;
    private Long productId;
    @Builder(builderMethodName = "childBuilder")

    public DisposeOfCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String description, Double quantity, Long productId) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.description = description;
        this.quantity = quantity;
        this.productId = productId;
    }
}
