package uz.personal.criteria.product;

import lombok.*;
import uz.personal.criteria.GenericCriteria;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StoresCriteria extends GenericCriteria {
    private String name;
    private String location;
    @Builder(builderMethodName = "childBuilder")

    public StoresCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String name, String location) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.name = name;
        this.location = location;
    }
}
