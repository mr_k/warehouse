package uz.personal.criteria.product;

import lombok.*;
import uz.personal.criteria.GenericCriteria;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductCriteria extends GenericCriteria {

    private String name;
    private String category;
    private String subcategory;
    private String description;
    private Double size;
    private String barcode;
    private String vendorCode;
    private Double uzs_price;
    private Double delivery_price_usd;
    private Double quantity;
    private Long warehouseId;

    @Builder(builderMethodName = "childBuilder")
    public ProductCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String name, String category, String description, String subcategory, Double size, String barcode, String venderCode, Double delivery_price_usd,Double uzs_price, Double quantity, Long warehouseId) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.name = name;
        this.category = category;
        this.description = description;
        this.subcategory = subcategory;
        this.size = size;
        this.barcode = barcode;
        this.vendorCode = venderCode;
        this.delivery_price_usd = delivery_price_usd;
        this.uzs_price = uzs_price;
        this.quantity=quantity;
        this.warehouseId=warehouseId;
    }
}
