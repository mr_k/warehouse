package uz.personal.dto.notes;

import lombok.*;
import uz.personal.domain.notes.File;
import uz.personal.dto.GenericDto;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FileDto extends GenericDto {
    private String name;
    @Builder(builderMethodName = "childBuilder")

    public FileDto(Long id, String name) {
        super(id);
        this.name = name;
    }
}
