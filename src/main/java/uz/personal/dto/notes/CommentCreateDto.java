package uz.personal.dto.notes;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.dto.GenericCrudDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentCreateDto extends GenericCrudDto {
    @ApiModelProperty(required = true)
    private String title;
    @ApiModelProperty(required = true)
    private String comment;
    @ApiModelProperty(required = true)
    private Long fileId;
}
