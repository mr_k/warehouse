package uz.personal.dto.notes;

import lombok.*;
import uz.personal.domain.notes.File;
import uz.personal.dto.GenericDto;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CommentDto extends GenericDto {
    private String title;
    private String comment;
    private File file;
    @Builder(builderMethodName = "childBuilder")

    public CommentDto(Long id,String title, String comment, File file) {
        super(id);
        this.title=title;
        this.comment = comment;
        this.file = file;
    }
}
