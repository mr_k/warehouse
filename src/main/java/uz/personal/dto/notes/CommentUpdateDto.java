package uz.personal.dto.notes;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.dto.CrudDto;

import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "comment update request")
public class CommentUpdateDto implements CrudDto {
    @ApiModelProperty(required = true)
    private Long id;
    @Size(max = 40, message = " max size %s")
    private String title;
    @Size(max = 300, message = " max size %s")
    private String comment;
    @ApiModelProperty(required = true)
    private Long fileId;
}
