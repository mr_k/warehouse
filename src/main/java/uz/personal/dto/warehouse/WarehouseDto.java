package uz.personal.dto.warehouse;

import lombok.*;
import uz.personal.domain.product.Store;
import uz.personal.dto.GenericDto;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WarehouseDto extends GenericDto {
    private String name;
    private String location;
    private Store store;
    @Builder(builderMethodName = "childBuilder")
    public WarehouseDto(Long id, String name, String location, Store store) {
        super(id);
        this.name = name;
        this.location = location;
        this.store=store;
    }
}
