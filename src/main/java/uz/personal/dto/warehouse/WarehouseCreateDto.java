package uz.personal.dto.warehouse;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import uz.personal.dto.GenericCrudDto;

import javax.validation.constraints.Size;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WarehouseCreateDto extends GenericCrudDto {
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String name;
    @ApiModelProperty(required = true)
    private String location;
    @ApiModelProperty(required = true)
    private Long storeId;
}
