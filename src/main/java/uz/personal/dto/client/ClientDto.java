package uz.personal.dto.client;

import lombok.*;
import uz.personal.dto.GenericDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClientDto extends GenericDto {
    private String clientName;
    private String phoneNumber;
    private String email;
    private String address;
    private String interest;
    private String discountCard;
    private Double discountOnService;
   private String note;
    private String tags;
    @Builder(builderMethodName = "childBuilder")

    public ClientDto(Long id, String clientName, String phoneNumber, String email, String address, String interest, String discountCard, Double discountOnService, String note, String tags) {
        super(id);
        this.clientName = clientName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.interest = interest;
        this.discountCard = discountCard;
        this.discountOnService = discountOnService;
        this.note = note;
        this.tags = tags;
    }
}
