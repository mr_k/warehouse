package uz.personal.dto.client;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.dto.CrudDto;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "client update request")
public class ClientUpdateDto implements CrudDto {
    @ApiModelProperty(required = true)
    private Long id;
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String clientName;
    @ApiModelProperty(required = true,example = "+998977777777")
    @Size(max = 13, message = " max size %s")
    private String phoneNumber;
    @ApiModelProperty(required = true,example = "simple@gmail.com")
    private String email;
//    @ApiModelProperty(required = true)
    private String address;
//    @ApiModelProperty(required = true)
    private String interest;
//    @ApiModelProperty(required = true, example = "2900000000063")
    @Size(max = 15, message = " max size %s")
    private String discountCard;
//    @ApiModelProperty(required = true)
    private Double discountOnService;
//    @ApiModelProperty(required = true)
    private String note;
    private String tags;
}
