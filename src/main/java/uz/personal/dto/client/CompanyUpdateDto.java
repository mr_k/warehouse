package uz.personal.dto.client;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.dto.CrudDto;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "client update request")
public class CompanyUpdateDto implements CrudDto {
    @ApiModelProperty(required = true)
    private Long id;
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String companyName;
    @ApiModelProperty(required = true, example = "+998977777777")
    @Size(max = 13, message = " max size %s")
    private String phoneNumber;
    @ApiModelProperty(required = true ,example = "simple@gmail.com")
    private String email;
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String address;
    @ApiModelProperty(required = true)
    private String interest;
    @ApiModelProperty(required = true)
    private String primaryStateRegistrationNumber;
    @ApiModelProperty(required = true)
    private String iNN;
    @ApiModelProperty(required = true)
    @Size( message = " max size %s")
    private String checkpointNumber;
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String legalAddress;
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String director;
    @ApiModelProperty(required = true)
    private String nameOfTheBank;
    @ApiModelProperty(required = true)
    private String accountNumber;
    @ApiModelProperty(required = true)
    private String cBAN;
    @ApiModelProperty(required = true)
    private String bankIdentificationCode;
    @ApiModelProperty(required = true, example = "2900000000063")
    private String discountCard;
    @ApiModelProperty(required = true)
    private Double discountOnService;
    private String note;
    private String tags;
}

