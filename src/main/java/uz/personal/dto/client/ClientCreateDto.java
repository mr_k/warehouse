package uz.personal.dto.client;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.dto.GenericCrudDto;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientCreateDto extends GenericCrudDto {
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String clientName;
    @ApiModelProperty(required = true,example = "+998977777777")
    @Size(max = 13, message = " max size %s")
    private String phoneNumber;
    @ApiModelProperty(required = true,example = "simple@gmail.com")
    private String email;
    @ApiModelProperty(required = true)
    private String address;
    @ApiModelProperty(required = true)
    private String interest;
    @ApiModelProperty(required = true, example = "2900000000063")
    @Size(max = 15, message = " max size %s")
    private String discountCard;
    @ApiModelProperty(required = true)
    private Double discountOnService;
    @ApiModelProperty(required = true)
    private String note;
    private String tags;
}
