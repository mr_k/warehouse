package uz.personal.dto.client;

import lombok.*;
import uz.personal.dto.GenericDto;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CompanyDto extends GenericDto {
    private String companyName;
    private String phoneNumber;
    private String email;
    private String address;
    private String interest;
    private String primaryStateRegistrationNumber;
    private String iNN;
    private String checkpointNumber;
    private String legalAddress;
    private String director;
    private String nameOfTheBank;
    private String accountNumber;
    private String cBAN;
    private String bankIdentificationCode;
    private String discountCard;
    private Double discountOnService;
    private String note;
    private String tags;

    public CompanyDto(Long id, String companyName, String phoneNumber, String email, String address, String interest, String primaryStateRegistrationNumber, String iNN, String checkpointNumber, String legalAddress, String director, String nameOfTheBank, String accountNumber, String cBAN, String bankIdentificationCode, String discountCard, Double discountOnService, String note, String tags) {
        super(id);
        this.companyName = companyName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.interest = interest;
        this.primaryStateRegistrationNumber = primaryStateRegistrationNumber;
        this.iNN = iNN;
        this.checkpointNumber = checkpointNumber;
        this.legalAddress = legalAddress;
        this.director = director;
        this.nameOfTheBank = nameOfTheBank;
        this.accountNumber = accountNumber;
        this.cBAN = cBAN;
        this.bankIdentificationCode = bankIdentificationCode;
        this.discountCard = discountCard;
        this.discountOnService = discountOnService;
        this.note = note;
        this.tags = tags;
    }
}
