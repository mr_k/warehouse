package uz.personal.dto.inventory;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.dto.GenericCrudDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Inventory update request")
public class InventoryUpdateDto extends GenericCrudDto {
    @ApiModelProperty(required = true)
    private Long id;
    @ApiModelProperty(required = true)
    private Long productId;
    @ApiModelProperty
    private Long quantity;



}
