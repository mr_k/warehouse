package uz.personal.dto.inventory;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.dto.GenericCrudDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InventoryCreateDto extends GenericCrudDto {
    private Long quantity;
    @ApiModelProperty(required = true)
    private String description;
    @ApiModelProperty(required = true)
    private Long productId;
    private boolean sign;
}
