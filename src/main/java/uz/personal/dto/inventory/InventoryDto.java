package uz.personal.dto.inventory;

import lombok.*;
import uz.personal.domain.product.Product;
import uz.personal.domain.product.Store;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.dto.GenericDto;
import uz.personal.dto.product.ProductDto;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InventoryDto  extends GenericDto {
    private String description;
    private Long quantity;
    private Product product;
    @Builder(builderMethodName = "childBuilder")

    public InventoryDto(Long id, Long quantity,Product product, String description) {
        super(id);
        this.quantity = quantity;
        this.product = product;
        this.description=description;
    }
}
