package uz.personal.dto.product;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.dto.GenericCrudDto;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DisposeOfCreateDto extends GenericCrudDto {
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String description;
    @ApiModelProperty(required = true)
    private Double quantity;
    @ApiModelProperty(required = true)
    private Long productId;
}
