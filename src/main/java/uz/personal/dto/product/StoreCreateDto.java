package uz.personal.dto.product;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.dto.GenericCrudDto;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreCreateDto extends GenericCrudDto {
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String name;
    @ApiModelProperty(required = true)
    private String location;
}
