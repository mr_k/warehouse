package uz.personal.dto.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.dto.CrudDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Write off update request")
public class DisposeOfUpdateDto implements CrudDto {
    @ApiModelProperty(required = true)
    private Long id;
    @ApiModelProperty(required = true)
    private String description;
    @ApiModelProperty(required = true)
    private Double quantity;
    @ApiModelProperty(required = true)
    private Long productId;
}
