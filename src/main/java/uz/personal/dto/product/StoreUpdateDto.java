package uz.personal.dto.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.dto.CrudDto;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Stores update request")
public class StoreUpdateDto implements CrudDto {
    @ApiModelProperty(required = true)
    private Long id;
    @Size(max = 100, message = " max size %s")
    private String name;
    private String location;
}
