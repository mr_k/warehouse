package uz.personal.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.domain.product.Product;
import uz.personal.dto.GenericDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DisposeOfDto extends GenericDto {
private String description;
private Double quantity;
private Product product;
    @Builder(builderMethodName = "childBuilder")

    public DisposeOfDto(Long id, String description, Double quantity, Product product) {
        super(id);
        this.description = description;
        this.quantity = quantity;
        this.product = product;
    }
}
