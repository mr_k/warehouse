package uz.personal.dto.product;

import lombok.*;
import uz.personal.domain.product.Product;
import uz.personal.dto.GenericDto;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StoreDto extends GenericDto {
    private String name;
    private String location;

    @Builder(builderMethodName = "childBuilder")

    public StoreDto(Long id, String name, String location) {
        super(id);
        this.name = name;
        this.location = location;
    }
}
