package uz.personal.dto.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.personal.dto.CrudDto;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Product update request")
public class ProductUpdateDto implements CrudDto {
    @ApiModelProperty(required = true)
    private Long id;
    @Size(max = 100, message = " max size %s")
    @ApiModelProperty(required = true)
    private String name;
    @ApiModelProperty(required = true)
    private String category;
    @ApiModelProperty(required = true)
    private String description;
    @ApiModelProperty(required = true)
    private String subcategory;
    @ApiModelProperty(required = true)
    private Double size;
//    @ApiModelProperty(required = true)
    private String barcode;
    @ApiModelProperty(required = true)
    private String vendorCode;
    @ApiModelProperty(required = true)
    private Double uzs_price;
    @ApiModelProperty(required = true)
    private Double quantity;
    @ApiModelProperty(required = true)
    private Double delivery_price_usd;
}
