package uz.personal.dto.product;

import lombok.*;
import uz.personal.domain.warehouse.Warehouse;
import uz.personal.dto.GenericDto;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductDto extends GenericDto {
    private String name;
    private String category;
    private String description;
    private String subcategory;
    private Double size;
    private String barcode;
    private String vendorCode;
    private Double delivery_price_usd;
    private Double uzs_price;
    private Double quantity;
    private Warehouse warehouse;
    @Builder(builderMethodName = "childBuilder")
    public ProductDto(Long id, String name, String category, String description, String subcategory, Double size, String barcode, String vendorCode, Double uzs_price, Double delivery_price_usd,Double quantity, Warehouse warehouse ) {
        super(id);
        this.name = name;
        this.category = category;
        this.description = description;
       this.subcategory = subcategory;
        this.size = size;
        this.barcode = barcode;
        this.vendorCode = vendorCode;
        this.uzs_price= uzs_price;
        this.delivery_price_usd= delivery_price_usd;
        this.quantity=quantity;
        this.warehouse=warehouse;
    }
}

