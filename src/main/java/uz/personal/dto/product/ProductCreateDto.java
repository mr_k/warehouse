package uz.personal.dto.product;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.personal.dto.GenericCrudDto;

import javax.persistence.Column;
import javax.validation.constraints.Size;
@Data
@AllArgsConstructor
@NoArgsConstructor
    public class ProductCreateDto extends GenericCrudDto {
    @ApiModelProperty(required = true)
    @Size(max = 100, message = " max size %s")
    private String name;
    @ApiModelProperty(required = true)
    private String category;
    @ApiModelProperty(required = true)
    private String description;
    @ApiModelProperty(required = true)
    private String subcategory;
    @ApiModelProperty(required = true)
    private Double size;
//    @ApiModelProperty(required = true)
    private String barcode;
    @ApiModelProperty(required = true)
    private String vendorCode;
    private  Double uzs_price;
    private Double delivery_price_usd;
    @ApiModelProperty(required = true)
    private Double quantity;
    @ApiModelProperty(required = true)
    private Long warehouseId;


}
