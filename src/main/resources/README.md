# Product Service

## Definitions
A product comprises multiple pieces and each piece holds relevant information about the product.

### Product
A product is a founding unit of the product-service which is sellable to anyone in exchange for the money. Products can 
range from nail-cutters to tractors, from phones to houses, from drinks to jewelry, etc...

Here is a sample on how products might look like:

![](./doc/products.png)

Each product ships with certain specifications and mostly those specifications will be unique per product category.
For instance, a specification defined for the products under `/Phones and accessories/Smartphones/Apple` category will
stay the same for all similar products belonging to `Apple` product category.

An `Apple iPhone 12 Pro Max 128GB` smartphone specification would look like as follows:

![](./doc/specifications.png)


### Product Category
A product category is a section where an actual product belongs to, for instance, a product called 
`Apple iPhone Pro Max 64GB` belongs to `/Phones and accessories/Smartphones/Apple` category.

Here is a sample on how product categories might look like:

![](./doc/categories.png)


### Product Template
Product template is a template which makes a lot easier to fill out underlying product details. 
For example, the same template will apply to all products under `/Phones and accessories/Smartphones` category. 
So it would be extremely redundant to force all the sellers to provide their own values for each product. Besides, 
there will be lots of duplicate work too. In order to avoid such redundancy, a product template is needed for 
**each category**. In such case, only a single template will be created for all products under
`/Phones and accessories/Smartphones` category, and the same template will be enforced during the creation of all 
underlying products.

## Queries
There are certain steps need to be followed to create a product. It is only possible to add a new product when all those 
steps are fulfilled.

### Step 1 - Add all product categories
A product simply cannot be correlated to the category if none exists. Therefore, product categories should be all settled
before starting with a product addition.

API details: ``POST /api/product/category/v1/upload``

Visit [here](http://caliphbaba.lc:8008/swagger-ui.html#product-category-v-1-controller) for more information.

### Step 2 - Add product template
A product depends on a template in order to define product attributes.

Here is a sample of a product template API for `Apple iPhone 12 Pro Max`:

``POST /api/product/template/v1/add``
```json
{
    "categoryId": "4f6d03a1-84cf-4ea7-9411-ac5a7d4b16b5",
    "quantityProperties": {
      "0": "COLOR",
      "1": "SIZE"
    },
    "sections": [
        {
            "name": "Product properties",
            "fields": [
                {
                    "name": "Operating System",
                    "type": "Keyword"
                },
                {
                    "name": "Release Year",
                    "type": "Integer"
                },
                {
                    "name": "Colors",
                    "type": "Keyword"
                }
            ]
        },
        {
            "name": "Camera",
            "fields": [
                {
                    "name": "Back Camera Resolution",
                    "measurement": "MEGAPIXELS",
                    "type": "Integer"
                },
                {
                    "name": "Front Camera Resolution",
                    "measurement": "MEGAPIXELS",
                    "type": "Integer"
                },
                {
                    "name": "Max Frames Per Second",
                    "type": "Keyword"
                },
                {
                    "name": "Max Video Resolution",
                    "type": "Keyword"
                }
            ]
        },
        {
            "name": "Communication",
            "fields": [
                {
                    "name": "5G",
                    "type": "Boolean"
                },
                {
                    "name": "4G",
                    "type": "Boolean"
                },
                {
                    "name": "Wi-Fi",
                    "type": "Keyword"
                },
                {
                    "name": "Bluetooth Version",
                    "type": "Float"
                }
            ]
        },
        {
            "name": "Display",
            "fields": [
                {
                    "name": "Screen Size",
                    "type": "Integer"
                },
                {
                    "name": "Screen Resolution",
                    "type": "Keyword"
                },
                {
                    "name": "Pixel Density (PPI)",
                    "description": "PPI stands for Pixels per Inch. Higher pixel density makes the display sharper.",
                    "type": "Integer"
                },
                {
                    "name": "Screen Type",
                    "type": "Keyword"
                },
                {
                    "name": "Touch Screen",
                    "type": "Boolean"
                }
            ]
        },
        {
            "name": "Storage",
            "fields": [
                {
                    "name": "Internal Memory Size",
                    "measurement": "GIGABYTES",
                    "type": "Integer"
                },
                {
                    "name": "Memory Card Reader",
                    "type": "Boolean"
                }
            ]
        },
        {
            "name": "Processor",
            "fields": [
                {
                    "name": "Processor Cores",
                    "description": "Single = 1 \nDual = 2 \nQuad = 4 \nHexa = 6 \nOcto = 8 \nDeca = 10",
                    "type": "Keyword"
                },
                {
                    "name": "System on Chip (SoC)",
                    "type": "Keyword"
                }
            ]
        },
        {
            "name": "Power source",
            "fields": [
                {
                    "name": "Exchangeable Battery",
                    "description": "If you can open the back of the phone to exchange the battery without using tools.",
                    "type": "Boolean"
                },
                {
                    "name": "Wireless Charging",
                    "description": "If the phone can be charged without using a cable, on a charging pad for example.",
                    "type": "Boolean"
                },
                {
                    "name": "Wireless Charging Standard",
                    "description": "There's two different wireless charging standards, QI and PMA. They are not compatible with eachother, because the signals sent and recieved are different. Some phones are compatible with both standards though.",
                    "type": "Keyword"
                },
                {
                    "name": "Fast Charging",
                    "type": "Boolean"
                }
            ]
        },
        {
            "name": "Measures",
            "fields": [
                {
                    "name": "Height",
                    "measurement": "MILLIMETERS",
                    "type": "Float"
                },
                {
                    "name": "Width",
                    "measurement": "MILLIMETERS",
                    "type": "Float"
                },
                {
                    "name": "Depth",
                    "measurement": "MILLIMETERS",
                    "type": "Float"
                },
                {
                    "name": "Weight",
                    "measurement": "GRAMS",
                    "type": "Float"
                }
            ]
        }
    ]
}
```
Above request will return a **product template ID** which can be used to create an actual underlying product.

### Step 3 - Add product
Now when categories are added on Step 1, and a template is added on Step 2, a new product can be added.

Here is how `Apple iPhone 12 Pro Max 128GB` product add request would look like:
``POST /api/product/v1/add``
```json
{
  "name": "Apple iPhone 12 Pro Max",
  "description": "iPhone 12 Pro Max smartphone. 64GB, 128GB are available",
  "price": 660,
  "shippingPrice": 0,
  "currency": "USD",
  "colors": ["White", "Black", "Green", "Grey"],
  "sellerId": "6d03c61c-8e32-4bef-b125-bf8022124d24",
  "templateId": "f132cea2-7b52-4490-8622-fca648bc97f0",
  "baseImage": "2021/09/16/UNqGdwo_iphone 13.png",
  "thumbnail": "2021/09/16/mRAK4MD_thumbnail_iphone 13.png",
  "categoryId": "525d8933-046a-4be2-b0f5-6cade538ab4d",
  "categories": ["7631b9fc-a0d1-41bd-946a-a42da510aea9", "69f5a320-086d-48ac-a9c0-15fa5e3e1bd0", "525d8933-046a-4be2-b0f5-6cade538ab4d"],
  "quantities": [
    {
      "property": "COLOR",
      "name": "black",
      "children": [
        {
          "property": "SIZE",
          "name": "128GB",
          "amount": 10,
          "price": 780,
          "image": "2021/09/16/black-iphone.png"
        },
        {
          "property": "SIZE",
          "name": "64GB",
          "amount": 30,
          "price": 660,
          "image": "2021/09/16/black-iphone-2.png"
        }
      ]
    },
    {
      "property": "COLOR",
      "name": "white",
      "children": [
        {
          "property": "SIZE",
          "name": "128GB",
          "amount": 50,
          "price": 780,
          "image": "2021/09/16/white-iphone.png"
        },
        {
          "property": "SIZE",
          "name": "64GB",
          "amount": 20,
          "price": 660,
          "image": "2021/09/16/white-iphone-2.png"
        }
      ]
    },
    {
      "property": "COLOR",
      "name": "grey",
      "children": [
        {
          "property": "SIZE",
          "name": "128GB",
          "amount": 10,
          "price": 780,
          "image": "2021/09/16/grey-iphone.png"
        }
      ]
    },
    {
      "property": "COLOR",
      "name": "green",
      "children": [
        {
          "property": "SIZE",
          "name": "64GB",
          "amount": 40,
          "price": 660,
          "image": "2021/09/16/green-iphone.png"
        }
      ]
    }
  ],
  "sections": [
    {
      "name": "Product properties",
      "attributes": [
        {
          "name": "Operating System",
          "values": [
            {
              "value": "iOS"
            }
          ]
        },
        {
          "name": "Release Year",
          "values": [
            {
              "value": "2019"
            }
          ]
        },
        {
          "name": "Colors",
          "values": [
            {
              "value": "White"
            },
            {
              "value": "Black"
            },
            {
              "value": "Gold"
            },
            {
              "value": "Grey"
            },
            {
              "value": "Green"
            }
          ]
        }
      ]
    },
    {
      "name": "Camera",
      "attributes": [
        {
          "name": "Back Camera Resolution",
          "values": [
            {
              "measurement": "MEGAPIXELS",
              "value": "12"
            }
          ]
        },
        {
          "name": "Front Camera Resolution",
          "values": [
            {
              "measurement": "MEGAPIXELS",
              "value": "8"
            }
          ]
        },
        {
          "name": "Max Frames Per Second",
          "values": [
            {
              "value": "240"
            }
          ]
        },
        {
          "name": "Max Video Resolution",
          "values": [
            {
              "value": "4K"
            }
          ]
        }
      ]
    },
    {
      "name": "Communication",
      "attributes": [
        {
          "name": "5G",
          "values": [
            {
              "value": "true"
            }
          ]
        },
        {
          "name": "4G",
          "values": [
            {
              "value": "true"
            }
          ]
        },
        {
          "name": "Wi-Fi",
          "values": [
            {
              "value": "Wi-Fi 4"
            },
            {
              "value": "Wi-Fi 5"
            },
            {
              "value": "Wi-Fi 6"
            }
          ]
        },
        {
          "name": "Bluetooth Version",
          "values": [
            {
              "value": "5.0"
            }
          ]
        }
      ]
    },
    {
      "name": "Display",
      "attributes": [
        {
          "name": "Screen Size",
          "values": [
            {
              "value": "6.7"
            }
          ]
        },
        {
          "name": "Screen Resolution",
          "values": [
            {
              "value": "2778x1284"
            }
          ]
        },
        {
          "name": "Pixel Density (PPI)",
          "values": [
            {
              "value": "458"
            }
          ]
        },
        {
          "name": "Screen Type",
          "values": [
            {
              "value": "OLED"
            }
          ]
        },
        {
          "name": "Touch Screen",
          "values": [
            {
              "value": "true"
            }
          ]
        }
      ]
    },
    {
      "name": "Storage",
      "attributes": [
        {
          "name": "Internal Memory Size",
          "values": [
            {
              "measurement": "GIGABYTES",
              "value": "128"
            }
          ]
        },
        {
          "name": "Memory Card Reader",
          "values": [
            {
              "value": "true"
            }
          ]
        }
      ]
    },
    {
      "name": "Processor",
      "attributes": [
        {
          "name": "Processor Cores",
          "values": [
            {
              "value": "Quad Core"
            }
          ]
        },
        {
          "name": "System on Chip (SoC)",
          "values": [
            {
              "value": "Apple A14"
            }
          ]
        }
      ]
    },
    {
      "name": "Power source",
      "attributes": [
        {
          "name": "Exchangeable Battery",
          "values": [
            {
              "value": "false"
            }
          ]
        },
        {
          "name": "Wireless Charging",
          "values": [
            {
              "value": "true"
            }
          ]
        },
        {
          "name": "Wireless Charging Standard",
          "values": [
            {
              "value": "QI"
            }
          ]
        },
        {
          "name": "Fast Charging",
          "values": [
            {
              "value": "true"
            }
          ]
        }
      ]
    },
    {
      "name": "Measures",
      "attributes": [
        {
          "name": "Height",
          "values": [
            {
              "measurement": "MILLIMETERS",
              "value": "160.8"
            }
          ]
        },
        {
          "name": "Width",
          "values": [
            {
              "measurement": "MILLIMETERS",
              "value": "78.1"
            }
          ]
        },
        {
          "name": "Depth",
          "values": [
            {
              "measurement": "MILLIMETERS",
              "value": "7.4"
            }
          ]
        },
        {
          "name": "Weight",
          "values": [
            {
              "measurement": "GRAMS",
              "value": "226.0"
            }
          ]
        }
      ]
    }
  ]
}
```

### Product checkout
Product checkout allows to make the payment for all products in a shopping cart. 

Here how the query looks like when there is a `White iPhone 12 Pro Max 64GB` in a shopping cart:
```json
{
    "products": [
        {
            "productId": "fd95e041-25cd-40cd-8e86-361c5da6374e",
            "orderId": "b4829430-f22c-456c-a707-60526f340abc",
            "quantities": [
                {
                    "property": "COLOR",
                    "name": "white",
                    "children": [
                        {
                            "property": "SIZE",
                            "name": "64GB",
                            "amount": 1
                        }
                    ]
                }
            ]
        }
    ]
}
```

As you see above, the query can be used to check out all products in a shopping cart. `orderId` defines the shopping cart.

### Product direct checkout
Product direct checkout is used when the buyer pays for the product directly in the product page. So in such case
the product won't be added in a shopping cart, there will be instant payment instead.

Here how the query looks like for a `Green iPhone 12 Pro Max 64GB`:
```json
{
    "productId": "fd95e041-25cd-40cd-8e86-361c5da6374e",
    "buyerId": "a4829430-f22c-456c-a707-60526f340fde",
    "deliveryType": "STANDARD",
    "deliveryInsured": true,
    "quantities": [
        {
            "property": "COLOR",
            "name": "green",
            "children": [
                {
                    "property": "SIZE",
                    "name": "64GB",
                    "amount": 1
                }
            ]
        }
    ]
}
```